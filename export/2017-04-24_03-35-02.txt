////////////////////////////////////////

Electric Piano

Timestamp = 2017-04-24_03-35-02

OSC Waveform     = 0
FM  Waveform     = 0
OSC FreqRatio    = 1
FM  FreqRatio    = 10
Modulation Depth = 2.2

[OSC Envelope]

Attack time   = 4
Peak Time     = 20
Decay Time    = 1900
Release Time  = 1000
Peak Level    = 0.8
Sustain Level = 0

[FM Envelope]

Attack time   = 0
Peak Time     = 0
Decay Time    = 360
Release Time  = 0
Peak Level    = 1
Sustain Level = 0.3

