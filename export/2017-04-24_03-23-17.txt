////////////////////////////////////////

Timestamp = 2017-04-24_03-23-17OSC Waveform     = 0
FM  Waveform     = 0
OSC FreqRatio    = 1
FM  FreqRatio    = 4
Modulation Depth = 1

[OSC Envelope]

Attack time   = 4
Peak Time     = 20
Decay Time    = 2000
Release Time  = 1000
Peak Level    = 0.8
Sustain Level = 0

[FM Envelope]

Attack time   = 0
Peak Time     = 0
Decay Time    = 30
Release Time  = 0
Peak Level    = 1
Sustain Level = 0.5

