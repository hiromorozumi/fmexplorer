INCLUDE=./include
LIBPATH=./lib

GTKCFLAGS=-mms-bitfields -IC:/mingw32/include/gtk-3.0 -IC:/mingw32/include/atk-1.0 -IC:/mingw32/include/cairo -IC:/mingw32/include/gdk-pixbuf-2.0 -IC:/mingw32/include/pango-1.0 -IC:/mingw32/include/glib-2.0 -IC:/mingw32/lib/glib-2.0/include -IC:/mingw32/include -IC:/mingw32/include/pixman-1 -IC:/mingw32/include/freetype2 -IC:/mingw32/include/libpng15

GTKLIBS=-Wl,-luuid -LC:/mingw32/lib -lgtk-3 -lgdk-3 -limm32 -lshell32 -lole32 -latk-1.0 -lgio-2.0 -lpangocairo-1.0 -lgdk_pixbuf-2.0 -lcairo-gobject -lpangoft2-1.0 -lpangowin32-1.0 -lgdi32 -lfreetype -lfontconfig -lpango-1.0 -lm -lcairo -lgobject-2.0 -lglib-2.0 -lintl

FMExplorer:
	g++ -I$(INCLUDE) $(GTKCFLAGS) hirolib.cpp WTable.cpp FM.cpp OSC.cpp Player.cpp Audio.cpp FMExplorer.cpp -L$(LIBPATH) $(LIBPATH)/portaudio_x86.lib $(GTKLIBS) -o FMExplorer.exe

debug:
	g++ -g -I$(INCLUDE) $(GTKCFLAGS) hirolib.cpp WTable.cpp FM.cpp OSC.cpp Player.cpp Audio.cpp FMExplorer.cpp -L$(LIBPATH) $(LIBPATH)/portaudio_x86.lib $(GTKLIBS) -o FMExplorer.exe
	
	
clean:
	rm -f FM_test.exe
	rm -f FMExplorer.exe