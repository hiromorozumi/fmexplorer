#ifndef AUDIO_H
#define AUDIO_H

#include "portaudio_win32/portaudio.h"
#include "Player.h"

class Audio
{	
	
public:

	static const int SAMPLE_RATE;
	static const int FRAMES_PER_BUFFER;

	Player* player;

	PaStreamParameters	inputParameters;
	PaStreamParameters  outputParameters;
	PaStream 			*stream;
	PaError 			err;
	bool				errorRaised;


// constructor
Audio();
~Audio(){}

// real callback function
int audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							);

// portaudio will call this function on callback. then reroute to real callback func.
static int audioCallbackReroute(
		const 
		void *input, void *output,
		unsigned long frameCount,
		const PaStreamCallbackTimeInfo* timeInfo,
		PaStreamCallbackFlags statusFlags,
		void *userData )
{
		return ((Audio*)userData)
			->audioCallback(input, output, frameCount, timeInfo, statusFlags);
}

void initialize();
void start();
void terminate();
void raiseError(PaError e);
void bindPlayer(Player* p);

};


#endif // AUDIO_H