// WTable.h /////////////////////////////////////////////
// WTable class - definition ////////////////////////////

#ifndef WTABLE_H
#define WTABLE_H

#include <cmath>

class WTable
{
public:
	
	static constexpr int WTABLE_SIZE = 4096;
	static constexpr float TWO_PI = 6.283185307;
	int currentTableOSC;
	int currentTableFM;
	bool tablesSetAlready;

	float table[WTABLE_SIZE][16];
	
	
	WTable();
	~WTable(){}
	
	void chooseTableOSC(int n);
	void chooseTableFM(int n);
	float readTableOSC(int phase);
	float readTableFM(int phase);
	void setAllTables();
};

#endif // WTABLE_H