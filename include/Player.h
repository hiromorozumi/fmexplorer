#ifndef PLAYER_H
#define PLAYER_H

#ifdef _WIN32
	#include <windows.h>
#endif

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

#include "portaudio_win32/portaudio.h"
#include "OSC.h"
// #include "hirolib.h"
// #include "FM.h"

//////////////////////////////////////////////////////////////////

struct Patch
{	
	std::string name;
	int OSCWaveform;
	int OSCAttackTime;
	int OSCPeakTime;
	int OSCDecayTime;
	int OSCReleaseTime;
	float OSCPeakLevel;
	float OSCSustainLevel;
	double OSCFreqRatio;
	int FMWaveform;
	int FMAttackTime;
	int FMPeakTime;
	int FMDecayTime;
	int FMReleaseTime;
	float FMPeakLevel;
	float FMSustainLevel;
	double FMFreqRatio;
	double modDepth;	
};

class Player
{
public:

	static const std::string SL;

	OSC osc;
	
	double semitoneRatio;
	double middleC;
	double cZero;
	
	std::vector<Patch> patch;
	int nPresets;
	int currentPreset;
	
	std::string presetName;
	int OSCWaveform;
	int OSCAttackTime;
	int OSCPeakTime;
	int OSCDecayTime;
	int OSCReleaseTime;
	float OSCPeakLevel;
	float OSCSustainLevel;
	double OSCFreqRatio;
	int FMWaveform;
	int FMAttackTime;
	int FMPeakTime;
	int FMDecayTime;
	int FMReleaseTime;
	float FMPeakLevel;
	float FMSustainLevel;
	double FMFreqRatio;
	double modDepth;
	
	Player();
	~Player(){}

	void initialize();
	void setNote(int toneNum);
	double getFrequency(int toneNum);
	void noteOn();
	void noteOff();
	float getMix();
	void setFMInstrument(int preset); // depreciate...
	void choosePreset(int n);
	void loadPresets();
	std::string getpwd();
	bool dirExists(const std::string &dirPath);
	
	OSC* getOSCObject();
	FM* getFMObject();
};

#endif // PLAYER_H