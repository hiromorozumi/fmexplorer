// FM.h /////////////////////////////////////////////
// FM class - definition ////////////////////////////

#ifndef FM_H
#define FM_H

#include "WTable.h"

class FM
{
public:

	WTable* wtable;

	static const int FM_TABLE_SIZE;
	static const int ENV_TABLE_SIZE;
	static const double FM_SAMPLE_RATE;

	int tableType;
	float yFlip;
	double phase;
	double increment;
	double freqRatio;
	double freq;
	double adjustedFreq;
	float gain;
	
	bool resting;
	int nEnvFrames;
	int nAttackFrames;
	int nPeakFrames;
	int nDecayFrames;
	int decayStartPos;
	float peakLevel;
	float decayAmount;
	float sustainLevel;
	float levelAtNoteOff;
	int envPos;
	bool envADfinished;	
	int nReleaseFrames;
	int releasePos;
	bool envRfinished;
	
	float modulationDepth;
	
	// DEBUG
	float lastValue;
	bool increasing;
	
	FM();
	~FM(){}
	
	void bindWTable(WTable* wt);
	void setTable(int type);
	void setGain(float g);
	float getGain();
	void advance();	
	void setToRest();
	void setNewNote(double newFreq);
	void setFrequency(double noteFreq);
	void setIncrement(double noteFreq);
	void setRiseToDefault();
	void setAttackTime(int attackTimeMS);
	void setPeakTime(int peakTimeMS);
	void setDecayTime(int decayTimeMS);
	void setReleaseTime(int releaseTimeMS);
	void setPeakLevel(float peakLV);
	void setSustainLevel(float sustainLV);
	void setEnvelope(int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
						float peakLV, float sustainLV);
	void advanceEnvelope();
	void refreshEnvelope();
	float getEnvelopeOutput();
	void readjustEnvParams();
	void initializePhase();
	void refreshForSongBeginning();
	float getOutput();
	void flipYAxis();
	void resetYFlip();
	
	float process(float input);
	void setFreqRatio(double ratio);
	void setModulationDepth(double depth);
};


#endif // FM_H
