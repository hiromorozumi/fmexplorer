#ifndef HIROLIB_H
#define HIROLIB_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>

#if defined (_WIN32)

	#include <windows.h>
	#include <conio.h>
	
#elif defined (__APPLE__) || defined (__linux__)

	#include <cstdlib>
	#include <unistd.h>
	#include <sys/select.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <dirent.h>	
	#include <termios.h>
	#include <time.h>
	#include <pwd.h>
	#include <cstring>

#endif


//////////////////////////////////////////////////////////////////////////////////


namespace kbd
{		
	//////////////////////////////////////////////////////////////////////
	//
	//		windows
	//
	//////////////////////////////////////////////////////////////////////
	
	void waitForRelease();
	void noWaitForRelease();
	bool up();
	bool down();
	bool left();
	bool right();
	bool space();		
	bool ret();
	bool ctrl();
	bool alt();
	bool tab();
	bool esc();
	bool escape();	
	bool f1();
	bool f2();
	bool f3();
	bool f4();
	bool f5();
	bool num0();
	bool num1();
	bool num2();
	bool num3();
	bool num4();
	bool num5();
	bool num6();
	bool num7();
	bool num8();
	bool num9();
	bool a();
	bool b();
	bool c();
	bool d();
	bool e();
	bool f();
	bool g();
	bool h();
	bool i();
	bool j();
	bool k();
	bool l();
	bool m();
	bool n();
	bool o();
	bool p();
	bool q();
	bool r();
	bool s();
	bool t();
	bool u();
	bool v();
	bool w();
	bool x();
	bool y();
	bool z();
	bool comma();
	bool ctrl_c();
	
	void update();	// used in mac OSX, blank func in Windows
	void wait_key_n();
	bool breakWasRequested();
	void flush();

#if defined (__APPLE__) || defined (__linux__)

	void reset_terminal_mode();	
	void set_conio_terminal_mode();
	
	int kbhit();	
	int getch();
	void init();
	bool inBuffer(int keyCode);

	// DEBUG
	int getKeyCodePressed();

#endif

}	// namespace kbd


////////////////////////////////////////////////////////////////////////////////////////


namespace scr
{
	
	void clear();
	void out(const std::string &str);
	void println(const std::string &str);

}	// namespace scr


//////////// MISC ///////////////////////////

std::string toStr(std::string &str);
std::string toStr(int n);
std::string toStr(double n);
std::string toStr(float n);
std::string toStr(long n);


//////////// timer //////////////////////////


namespace timer
{

void resetTimer();
double getElapsedTime();	

#if defined (__APPLE__) || defined (__linux__)

	unsigned long getTimeMicroSec();

#endif

}	// namespace timer


/////////////// filesystem //////////////////


namespace file
{

	std::string getpwd();
	std::vector<std::string> getFileNamesInDir(const std::string &dirPath);
	std::vector<std::string> getDirNamesInDir(const std::string &folder);	
	bool makeDir(const std::string &dirPath);
	bool copyFile(const std::string &sourcePath, const std::string &destPath);
	bool dirExists(const std::string &dirPath);

} // namespace file


//////////// initialization /////////////////


void hirolibInit();


////////////////////////////////////////////


#endif // HIROLIB_H
