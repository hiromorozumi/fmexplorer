#include <iostream>
#include <string>
#include <cmath>

#include "portaudio_win32/portaudio.h"
#include "OSC.h"
#include "hirolib.h"

using namespace std;

//////////////////////////////////////////////////////////////////

class Player
{
public:

	OSC osc;
	
	double semitoneRatio;
	double middleC;
	double cZero;
	
	Player();
	~Player(){}

	void setNote(int toneNum);
	double getFrequency(int toneNum);
	void noteOn();
	void noteOff();
	float getMix();
	void setFMInstrument(int preset);
};

//////////////////////////////////////////////////////////////////

Player::Player()
{
	semitoneRatio = pow(2, 1.0/12.0);
	middleC = 220.0 * pow(semitoneRatio, 3); // middle C is C4
	cZero = middleC * pow(0.5, 4.0);
	noteOff();
}

void Player::setNote(int toneNum)
{
	double freqNewNote = getFrequency(toneNum);
	osc.setNewNote(freqNewNote);
}

double Player::getFrequency(int toneNum)
{
	return cZero * pow(semitoneRatio, toneNum);
}

void Player::noteOn()
{
	
}

void Player::noteOff()
{
	osc.setToRest();
}

float Player::getMix() // 0 for left, 1 for right
{
	float out = osc.getOutput();
	osc.advance();
	return out;
}

void Player::setFMInstrument(int preset)
{
	if(preset==0) // default
	{
		osc.setTable(0);
		osc.setFMTable(0);
		
		// envelope params...
		// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
		//				float peakLV, float sustainLV);
		
		osc.setEnvelope  (0, 0, 0, 0,    1.0f, 1.0f);
		osc.setFMEnvelope(0, 0, 0, 0,    1.0f, 1.0f);
		osc.setOSCFreqRatio(1.0);
		osc.setFMFreqRatio(1.0);
		osc.setFMModulationDepth(1.0);
		
	}
	else if(preset==1) // electric piano
	{
		osc.setTable(0);
		osc.setFMTable(0);
		
		// envelope params...
		// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
		//				float peakLV, float sustainLV);
		
		osc.setEnvelope  (4, 20, 2000, 1000, 0.8f, 0.0f);
		osc.setFMEnvelope(0, 0,  30,  0,  1.0f, 0.5f);
		osc.setOSCFreqRatio(2.0);
		osc.setFMFreqRatio(0.5);
		osc.setFMModulationDepth(2.0);
	}
	else if(preset==2)
	{
		osc.setTable(0);
		osc.setFMTable(2);
		
		// envelope params...
		// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
		//				float peakLV, float sustainLV);
		
		osc.setEnvelope  (0, 0, 500, 200, 0.8f, 0.5f);
		osc.setFMEnvelope(0, 0,  30,  0,  1.0f, 1.0f);
		osc.setOSCFreqRatio(1.0);
		osc.setFMFreqRatio(2.0);
		osc.setFMModulationDepth(1.4);
	}
	
}

//////////////////////////////////////////////////////////////////

class Audio
{	
	
public:

	static const int SAMPLE_RATE;
	static const int FRAMES_PER_BUFFER;

	Player* player;

	PaStreamParameters	inputParameters;
	PaStreamParameters  outputParameters;
	PaStream 			*stream;
	PaError 			err;
	bool				errorRaised;


// constructor
Audio();
~Audio(){}

// real callback function
int audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							);

// portaudio will call this function on callback. then reroute to real callback func.
static int audioCallbackReroute(
		const 
		void *input, void *output,
		unsigned long frameCount,
		const PaStreamCallbackTimeInfo* timeInfo,
		PaStreamCallbackFlags statusFlags,
		void *userData )
{
		return ((Audio*)userData)
			->audioCallback(input, output, frameCount, timeInfo, statusFlags);
}

void initialize();
void start();
void terminate();
void raiseError(PaError e);
void bindPlayer(Player* p);

};


//////////////////////////////////////

const int Audio::SAMPLE_RATE = 44100;
const int Audio::FRAMES_PER_BUFFER = 128;

Audio::Audio()
{
	initialize();
}

// real callback function
int Audio::audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							)
{	
	float *out = (float*)(outputBuffer);
	int nBufferFrames = (int)framesPerBuffer;

	(void) inputBuffer;
	(void) timeInfo;
	(void) statusFlags;
	
	for(int i=0; i<nBufferFrames; i++)
	{
		// float r = ((float) rand() / (RAND_MAX));
		// r -= 0.5f;
		*out = player->getMix();
		out++;	
	}
	
	return paContinue;
}

void Audio::initialize()
{	
	errorRaised = false;
	err = Pa_Initialize();
	if( err != paNoError ) raiseError(err);
	
	// set up output parameters
    outputParameters.device = Pa_GetDefaultOutputDevice(); // use default device
    if (outputParameters.device == paNoDevice)
	{
      cout << "Error: No default output device." << "\r\n";
    }
    outputParameters.channelCount = 1; // mono
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
	
	// open audio stream for recording using default device
	err = Pa_OpenStream(
		&stream,
		NULL, // no input
		&outputParameters,
		SAMPLE_RATE,
		FRAMES_PER_BUFFER,
		paClipOff,
		audioCallbackReroute,
		this
	);
	
	if( err != paNoError ) raiseError(err);
	
	cout << "Audio iniialized!\n";
}

void Audio::start()
{
	// start the audio stream!
	err = Pa_StartStream(stream);
	if(err != paNoError) raiseError(err);
}

void Audio::terminate()
{
	err = Pa_CloseStream( stream );
	if(err != paNoError) raiseError(err);	
	err = Pa_Terminate( );
	if(err != paNoError) raiseError(err);	
}

void Audio::raiseError(PaError e)
{
	cout << "Port audio error:\n";
	string errText = Pa_GetErrorText(e);
	cout << errText << "\r\n";
	errorRaised = true;
}

void Audio::bindPlayer(Player* p)
{
	player = p;
}

//////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
	bool exitApp = false;
	int patch = 0;
	Player player;
	player.setFMInstrument(0);
	Audio audio;
	audio.bindPlayer(&player);
	audio.start();
	
	scr::clear();
	
	while(!exitApp)
	{
		if(kbd::esc())
		{
			while(kbd::esc()){}
			exitApp = true;
		}
		else if(kbd::z())
		{	player.setNote(48); while(kbd::z()){} }
		else if(kbd::s())
		{	player.setNote(49); while(kbd::s()){} }		
		else if(kbd::x())
		{	player.setNote(50); while(kbd::x()){} }
		else if(kbd::d())
		{	player.setNote(51); while(kbd::d()){} }		
		else if(kbd::c())
		{	player.setNote(52); while(kbd::c()){} }
		else if(kbd::v())
		{	player.setNote(53); while(kbd::v()){} }
		else if(kbd::g())
		{	player.setNote(54); while(kbd::g()){} }
		else if(kbd::b())
		{	player.setNote(55); while(kbd::b()){} }		
		else if(kbd::h())
		{	player.setNote(56); while(kbd::h()){} }
		else if(kbd::n())
		{	player.setNote(57); while(kbd::n()){} }		
		else if(kbd::j())
		{	player.setNote(58); while(kbd::j()){} }
		else if(kbd::m())
		{	player.setNote(59); while(kbd::m()){} }		
		else if(kbd::comma())
		{	player.setNote(60); while(kbd::comma()){} }	
		else if(kbd::ret())
		{
			while(kbd::ret()){}
			patch++;
			if(patch>2)
				patch = 0;
			player.setFMInstrument(patch);
		}
		else
			player.noteOff();
	}
	
	
	return 0;
}