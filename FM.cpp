/// FM class - Implementation /////////////////////////

#include <iostream>
#include <math.h>
#include "FM.h"

const int FM::FM_TABLE_SIZE = 4096;
const int FM::ENV_TABLE_SIZE = 1024;
const double FM::FM_SAMPLE_RATE = 44100.0;

using namespace std;

FM::FM()
{
	tableType = 1;
	
	modulationDepth = 1.0f;
	yFlip = 1.0f;
	phase = 0.0;
	increment = 0.0;
	freq = 10.0; // not to set to zero to safeguard
	adjustedFreq = 0;
	freqRatio = 1.0;
	gain = 1.0f; // default gain
	
	resting = false;
	
	nAttackFrames = 0; // nAttackFrames = 1000;
	nPeakFrames = 0; // nPeakFrames = 1000;
	nDecayFrames = 0; // nDecayFrames = 9600;
	
	nEnvFrames = nAttackFrames + nPeakFrames + nDecayFrames;
	decayStartPos = nAttackFrames + nPeakFrames;
	
	peakLevel = 1.0f;
	sustainLevel = 1.0f;
	levelAtNoteOff = sustainLevel;
	decayAmount = peakLevel - sustainLevel;
	envPos = 0;
	envADfinished = false;
	
	nReleaseFrames = 2000;
	releasePos = 0;
	envRfinished = true;
	
	// DEBUG
	lastValue = 0;
	increasing = true;
}

void FM::bindWTable(WTable* wt)
{
	wtable = wt;
}

void FM::setTable(int type)
{
	wtable->chooseTableFM(type);
}

void FM::setGain(float g)
	{ gain = g; }

float FM::getGain()
	{ return gain; }

void FM::advanceEnvelope()
{
	if(!resting) // currently playing a note
	{
		if(!envADfinished)
		{
			envPos++;
			if(envPos >= nEnvFrames)
				envADfinished = true;
		}
	}
	else // you are on a rest now
	{
		if(!envRfinished)
		{
			releasePos++;
			if(releasePos >= nReleaseFrames)
			{
				envRfinished = true;
			}
		}
	}
}

// go back to the beginning of envelope
void FM::refreshEnvelope()
{
	envPos = 0;
	envADfinished = false;
	releasePos = 0;
	envRfinished = false;
	levelAtNoteOff = 0.0f;
}

float FM::getEnvelopeOutput()
{
	float output;
	
	if(!resting) // when you're not on a rest - playing a note now
	{
		if(envPos < nAttackFrames) // in attack stage
		{
			output = peakLevel * 
			(static_cast<float>(envPos) / static_cast<float>(nAttackFrames));
		}
		else if(envPos < (nAttackFrames + nPeakFrames)) // in peak stage
		{
			output = peakLevel;
		}
		else if(envPos < nEnvFrames) // in decay stage
		{
			output = peakLevel - decayAmount * ( static_cast<float>(envPos - decayStartPos) / static_cast<float> (nDecayFrames) );
		}
		else if(envADfinished) // in sustain stage
		{
			output = sustainLevel;
		}
		
		levelAtNoteOff = output; // update this for when release stage kicks in
	}
	
	// if resting flag is on, means you're in release stage
	if(resting)
	{
		if(!envRfinished)
			output = levelAtNoteOff * ( static_cast<float>(nReleaseFrames - releasePos) / static_cast<float>(nReleaseFrames) );
		else
		{
			output = 0.0f;
			phase = 0; // reset phase for next note!
		}
	}
	
	return output;
}

void FM::setToRest()
{
	resting = true;
}

void FM::advance()
{
	// advance on the sample table
	phase += increment;	
	adjustedFreq = freq;
	
	while(phase >= FM_TABLE_SIZE)
	{
		phase -= FM_TABLE_SIZE;
	}
	
	// advance envelope also
	advanceEnvelope();
}

void FM::setNewNote(double newFreq)
{
	setFrequency(newFreq * freqRatio);
	// initializePhase();
	refreshEnvelope();
	resting = false;
}

// set the frequency and phase increment at once
void FM::setFrequency(double noteFreq)
{
	freq = noteFreq;
	adjustedFreq = freq;
	setIncrement(freq);
}

// set the phase increment to travel across wave table every frame
void FM::setIncrement(double noteFreq)
{
	// finally, set the phase increment
	increment = ( static_cast<double>(FM_TABLE_SIZE) / ( FM_SAMPLE_RATE / adjustedFreq ) );
	
	if(increment < 0)
		increment = 0;
}

void FM::setAttackTime(int attackTimeMS)
{
	nAttackFrames = static_cast<int> (FM_SAMPLE_RATE * attackTimeMS / 1000.0);
	readjustEnvParams();
}

void FM::setPeakTime(int peakTimeMS)
{
	nPeakFrames = static_cast<int> (FM_SAMPLE_RATE * peakTimeMS / 1000.0);
	readjustEnvParams();
}

void FM::setDecayTime(int decayTimeMS)
{
	nDecayFrames = static_cast<int> (FM_SAMPLE_RATE * decayTimeMS / 1000.0);
	readjustEnvParams();
}

void FM::setReleaseTime(int releaseTimeMS)
{ 
	nReleaseFrames = static_cast<int> (FM_SAMPLE_RATE * releaseTimeMS / 1000.0);
	readjustEnvParams();
}
	
void FM::setPeakLevel(float peakLV)
{
	peakLevel = peakLV;
	readjustEnvParams();
}

void FM::setSustainLevel(float sustainLV)
{
	sustainLevel = sustainLV;
	readjustEnvParams();
}	

void FM::setEnvelope(int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
						float peakLV, float sustainLV)
{
	setAttackTime(attackTimeMS);
	setPeakTime(peakTimeMS);
	setDecayTime(decayTimeMS);
	setReleaseTime(releaseTimeMS);
	setPeakLevel(peakLV);
	setSustainLevel(sustainLV);
	readjustEnvParams();
}

void FM::readjustEnvParams()
{
	nEnvFrames = nAttackFrames + nPeakFrames + nDecayFrames;
	decayStartPos = nAttackFrames + nPeakFrames;
	decayAmount = peakLevel - sustainLevel;	
}
	
void FM::initializePhase()
{
	phase = 0;
}

void FM::refreshForSongBeginning()
{
	initializePhase();
	refreshEnvelope();
}


float FM::getOutput()
{	
	float out;
	
	int ph = static_cast<int>(round(phase));
	if(ph>=FM_TABLE_SIZE)
		ph -= FM_TABLE_SIZE;

	out = wtable->readTableFM(ph);
	
	if(yFlip > 0)
		out *= getEnvelopeOutput();
	else
		out = -out * getEnvelopeOutput();
	
	out *= gain;
	
	return out;
}

float FM::process(float input)
{
	float out = input;
	
	// modulation depth ... 0 to 1, 0 = no change, 1 = maximum change
	float modBase = 1.0f + modulationDepth;
	
	// if modulation depth = 1.0,
	// diviation factor ranges from 0.5 (half) to 2.0 (double)
	
	// float diviationFactor = pow(modBase, getOutput());
	// out *= diviationFactor; // modulate!!
	
	// DEBUG ... ???
	// if modulation depth = 1.0, measure the distance to one octave below
	float freqDelta = ((input / 2.0f) * modulationDepth) * getOutput();
	out += freqDelta;
	
	// safeguarding...
	// if(out<20.0)out=20.0;
	// else if(out>44100.0) out = 44100.0;
	
	// DEBUG
	/*
	if(out<lastValue && increasing && input > 40.0 && input < 4000.0)
	{
		cout << out << " ";
		increasing = false;
	}
	else if(out>lastValue && !increasing && input > 40.0 && input < 4000.0)
	{
		cout << out << " ";
		increasing = true;
	}
	*/
	lastValue = out;
	
	advance();
	
	return out;
}

// set yFlip between 1.0 and -1.0
// determines if wavetable is read as is or vertically inverted
void FM::flipYAxis()
{
	yFlip = -1.0f;
}

// reset yFlip to default normal 1.0 (table reading won't get vertically inverted)
void FM::resetYFlip()
{
	yFlip = 1.0f;
}

void FM::setFreqRatio(double ratio)
{
	if(ratio<0) ratio = 1.0; // safeguard
	freqRatio = ratio;
}

void FM::setModulationDepth(double depth)
{	
	if(depth > 10.0) depth = 10.0;
	else if(depth < 0.0) depth = 0.0;
	modulationDepth = depth;
}
