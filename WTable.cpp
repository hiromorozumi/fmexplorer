/// WTable class - Implementation /////////////////////////

#include "WTable.h"

WTable::WTable()
{ 
	currentTableOSC = 0;
	currentTableFM = 0;
	tablesSetAlready = false;
}

void WTable::chooseTableOSC(int n)
{	currentTableOSC = n; }

void WTable::chooseTableFM(int n)
{	currentTableFM = n; }

float WTable::readTableOSC(int phase)
{	return table[phase][currentTableOSC]; }

float WTable::readTableFM(int phase)
{	return table[phase][currentTableFM]; }

void WTable::setAllTables()
{
	if(tablesSetAlready)
		return;
	
	float maxAmp;
	int oneCycleFrames;
	
	// sine table
		maxAmp = 1.0f;
		for(int i=0; i<WTABLE_SIZE; i++)
		{
			table[i][0] = sin( TWO_PI * (static_cast<float>(i) / static_cast<float>(WTABLE_SIZE) ) ) * maxAmp;
		}
		
	// square table
		for(int i=0; i<WTABLE_SIZE/2; i++)
		{
			table[i][1] = 0.80f;
		}
		for(int i=WTABLE_SIZE/2; i<WTABLE_SIZE; i++)
		{
			table[i][1] = -0.80f;
		}
	
	// sawthooth wave
		for(int i=0; i<WTABLE_SIZE; i++)
			table[i][2] = -0.99f  + (static_cast<float>(i) / static_cast<float>(WTABLE_SIZE)) * 1.98f;

	// triangle wave
		for(int i=0; i<WTABLE_SIZE/2; i++)
		{
			int index = WTABLE_SIZE/4 + i;
			table[index][3] = -0.99f  + (static_cast<float>(i) / static_cast<float>(WTABLE_SIZE/2)) * 1.98f;
		}				
		for(int i=WTABLE_SIZE/2; i<WTABLE_SIZE; i++)
		{
			int index = WTABLE_SIZE/4 + i;
			if(index>=WTABLE_SIZE) index -= WTABLE_SIZE;
			table[index][3] = 0.99f  - (static_cast<float>(i-WTABLE_SIZE/2) / static_cast<float>(WTABLE_SIZE/2)) * 1.98f;
		}

	// pulse wave 12.5%-87.5% ratio
		for(int i=0; i<WTABLE_SIZE/8; i++)
		{
			table[i][4] = -0.80f;
		}
		for(int i=WTABLE_SIZE/8; i<WTABLE_SIZE; i++)
		{
			table[i][4] = 0.80f;
		}					
		
	// pulse wave 25%-75% ratio		
		for(int i=0; i<WTABLE_SIZE/4; i++)
		{
			table[i][5] = -0.80f;
		}
		for(int i=WTABLE_SIZE/4; i<WTABLE_SIZE; i++)
		{
			table[i][5] = 0.80f;
		}		
		
	// pulse wave 33.3% ratio
		for(int i=0; i<WTABLE_SIZE/3; i++)
		{
			table[i][6] = -0.80f;
		}
		for(int i=WTABLE_SIZE/3; i<WTABLE_SIZE; i++)
		{
			table[i][6] = 0.80f;
		}				

	// limit...
	for(int j=0; j<16; j++)
	{
		for(int i=0; i<WTABLE_SIZE; i++)
		{
			if(table[i][j]>0.99f) table[i][j] = 0.99f;
			else if(table[i][j] < -0.99f) table[i][j] = -0.99f;
		}
	}
	
	// prevent this method from getting called from second time
	tablesSetAlready = true;
	
}