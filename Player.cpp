#include "Player.h"

#ifdef _WIN32
	const std::string Player::SL = "\\";
#else
	const std::string Player::SL = "/";	
#endif

using namespace std;

Player::Player()
{
	semitoneRatio = pow(2, 1.0/12.0);
	middleC = 220.0 * pow(semitoneRatio, 3); // middle C is C4
	cZero = middleC * pow(0.5, 4.0);
	noteOff();
	nPresets = 1;
}

void Player::initialize()
{
	loadPresets();
}

void Player::setNote(int toneNum)
{
	double freqNewNote = getFrequency(toneNum);
	osc.setNewNote(freqNewNote);
}

double Player::getFrequency(int toneNum)
{
	return cZero * pow(semitoneRatio, toneNum);
}

void Player::noteOn()
{
	
}

void Player::noteOff()
{
	osc.setToRest();
}

float Player::getMix() // 0 for left, 1 for right
{
	float out = osc.getOutput();
	osc.advance();
	return out;
}

void Player::setFMInstrument(int preset)
{
	if(preset==0 || preset>=3) // default
	{
		presetName = "Default";
		
		OSCWaveform = 0; FMWaveform = 0;
		OSCFreqRatio = 1.0; FMFreqRatio = 1.0;
		modDepth = 1.0;
		
		OSCAttackTime = 0; OSCPeakTime = 0; OSCDecayTime = 0; OSCReleaseTime = 0;
		OSCPeakLevel = 1.0f; OSCSustainLevel = 1.0f;		
		FMAttackTime = 0; FMPeakTime = 0; FMDecayTime = 0; FMReleaseTime = 0;
		FMPeakLevel = 1.0f; FMSustainLevel = 1.0f;
		
		osc.setTable(OSCWaveform); osc.setFMTable(FMWaveform);
		osc.setOSCFreqRatio(OSCFreqRatio); osc.setFMFreqRatio(FMFreqRatio);
		osc.setFMModulationDepth(modDepth);
		
		// envelope params...
		// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
		//				float peakLV, float sustainLV);
		
		osc.setEnvelope  (OSCAttackTime, OSCPeakTime, OSCDecayTime, OSCReleaseTime,
						OSCPeakLevel, OSCSustainLevel);
		osc.setEnvelope  (FMAttackTime, FMPeakTime, FMDecayTime, FMReleaseTime,
						FMPeakLevel, FMSustainLevel);
		
	}
	else if(preset==1) // electric piano
	{
		presetName = "Electric Piano";
		
		OSCWaveform = 0; FMWaveform = 0;
		OSCFreqRatio = 1.0; FMFreqRatio = 4.0;
		modDepth = 1.0;
		
		OSCAttackTime = 4; OSCPeakTime = 20; OSCDecayTime = 2000; OSCReleaseTime = 1000;
		OSCPeakLevel = 0.8f; OSCSustainLevel = 0.0f;		
		FMAttackTime = 0; FMPeakTime = 0; FMDecayTime = 30; FMReleaseTime = 0;
		FMPeakLevel = 1.0f; FMSustainLevel = 0.5f;
		
		osc.setTable(0);
		osc.setFMTable(0);
		
		osc.setTable(OSCWaveform); osc.setFMTable(FMWaveform);
		osc.setOSCFreqRatio(OSCFreqRatio); osc.setFMFreqRatio(FMFreqRatio);
		osc.setFMModulationDepth(modDepth);
		
		osc.setEnvelope  (OSCAttackTime, OSCPeakTime, OSCDecayTime, OSCReleaseTime,
						OSCPeakLevel, OSCSustainLevel);
		osc.setEnvelope  (FMAttackTime, FMPeakTime, FMDecayTime, FMReleaseTime,
						FMPeakLevel, FMSustainLevel);
	}
	else if(preset==2)
	{
		presetName = "Preset 2";		
		
		OSCWaveform = 0; FMWaveform = 0;
		OSCFreqRatio = 1.0; FMFreqRatio = 1.0;
		modDepth = 1.0;
		
		OSCAttackTime = 0; OSCPeakTime = 0; OSCDecayTime = 0; OSCReleaseTime = 0;
		OSCPeakLevel = 1.0f; OSCSustainLevel = 1.0f;		
		FMAttackTime = 0; FMPeakTime = 0; FMDecayTime = 0; FMReleaseTime = 0;
		FMPeakLevel = 1.0f; FMSustainLevel = 1.0f;
		
		osc.setTable(OSCWaveform); osc.setFMTable(FMWaveform);
		osc.setOSCFreqRatio(OSCFreqRatio); osc.setFMFreqRatio(FMFreqRatio);
		osc.setFMModulationDepth(modDepth);
		
		// envelope params...
		// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
		//				float peakLV, float sustainLV);
		
		osc.setEnvelope  (OSCAttackTime, OSCPeakTime, OSCDecayTime, OSCReleaseTime,
						OSCPeakLevel, OSCSustainLevel);
		osc.setFMEnvelope  (FMAttackTime, FMPeakTime, FMDecayTime, FMReleaseTime,
						FMPeakLevel, FMSustainLevel);
	}
	
}

// load a patch preset from 'patch' vector at index n
void Player::choosePreset(int n)
{
	if(n>=nPresets)
	{
		cout << "Preset number " << n << " does not exist!" << endl;
		return;
	}
	
	presetName = patch[n].name;
	
	OSCWaveform = patch[n].OSCWaveform; FMWaveform = patch[n].FMWaveform;
	OSCFreqRatio = patch[n].OSCFreqRatio; FMFreqRatio = patch[n].FMFreqRatio;
	modDepth = patch[n].modDepth;
	
	OSCAttackTime = patch[n].OSCAttackTime; OSCPeakTime = patch[n].OSCPeakTime; 
	OSCDecayTime = patch[n].OSCDecayTime; OSCReleaseTime = patch[n].OSCReleaseTime;
	OSCPeakLevel = patch[n].OSCPeakLevel; OSCSustainLevel = patch[n].OSCSustainLevel;		
	FMAttackTime = patch[n].FMAttackTime; FMPeakTime = patch[n].FMPeakTime; 
	FMDecayTime = patch[n].FMDecayTime; FMReleaseTime = patch[n].FMReleaseTime;
	FMPeakLevel = patch[n].FMPeakLevel; FMSustainLevel = patch[n].FMSustainLevel;	
	
	osc.setTable(OSCWaveform); osc.setFMTable(FMWaveform);
	osc.setOSCFreqRatio(OSCFreqRatio); osc.setFMFreqRatio(FMFreqRatio);
	osc.setFMModulationDepth(modDepth);
	
	// envelope params...
	// (int attackTimeMS, int peakTimeMS, int decayTimeMS, int releaseTimeMS,
	//				float peakLV, float sustainLV);
	
	osc.setEnvelope  (OSCAttackTime, OSCPeakTime, OSCDecayTime, OSCReleaseTime,
					OSCPeakLevel, OSCSustainLevel);
	osc.setFMEnvelope  (FMAttackTime, FMPeakTime, FMDecayTime, FMReleaseTime,
					FMPeakLevel, FMSustainLevel);

	currentPreset = n;
}

// read multiple presets from 'presets.txt'
void Player::loadPresets()
{
	string str = "";
	string strPath = getpwd();
	strPath += SL + "presets.txt";
	
	ifstream inFile(strPath);
	if(!inFile)
	{
		cout << "Error opening file: " << strPath << endl;
		return;
	}
	
	patch.clear();
	
	string line;
	while(std::getline(inFile, line))
	{
		str += line;
	}
	inFile.close();

	str += "$$$$$";
	int pos = 0;
	char ch = '\0';

	
	// take out all spaces
	/*
	while(ch!='$')
	{
		ch = str.at(pos);
		if(ch==' ')
			str.erase(pos, 1);
		else
			pos++;
	}
	*/
	
	// parse the string now...
	pos = 0;
	
	bool allDone = false;
	while(!allDone)
	{
		if(str.find("PRESET_BEGIN", pos)!=string::npos) // 'PRESET_BEGIN' found at pos
		{
			// then, find 'PRESET_END' to close this one preset chunk
			if(str.find("PRESET_END", pos)!=string::npos)
			{
				
				// temp storage...
				
				std::string _name = "";
				int _OSCWaveform = 0;
				int _FMWaveform = 0;
				double _OSCFreqRatio = 1.0;
				double _FMFreqRatio = 1.0;
				double _modDepth = 0.0;
				int _OSCAttackTime = 0;
				int _OSCPeakTime = 0;
				int _OSCDecayTime = 0;
				int _OSCReleaseTime = 0;
				float _OSCPeakLevel = 1.0f;
				float _OSCSustainLevel = 1.0f;
				int _FMAttackTime = 0;
				int _FMPeakTime = 0;
				int _FMDecayTime = 0;
				int _FMReleaseTime = 0;
				float _FMPeakLevel = 1.0f;
				float _FMSustainLevel = 1.0f;					
				
				int posStart = str.find("PRESET_BEGIN", pos) + 12;
				int posEnd = str.find("PRESET_END", pos);
				if(posEnd!=string::npos)
					pos = posEnd; 				// fast forward for next preset position!
				else
					pos = posStart;
				int nCharsToExtract = posEnd - posStart;
				string strPatch = str.substr(posStart, nCharsToExtract);
				strPatch += "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"; 
								// safeguard! 30 chars total - safe for substring-search up to 30 chars
				int pos2 = 0;
				bool presetDone = false;
				while(!presetDone)
				{
					//
					// TODO
					//
					//	makes more sense to search using substr and comparison at pos!
					//
					//
					
					
					if(strPatch.substr(pos2, 6)=="NAME='")
					{
						size_t begin = pos2 + 6;
						size_t end = strPatch.find("'", pos2+6);
						int nChars;
						if(end==string::npos)
							nChars = 1;
						else
							nChars = end - begin;
						
						_name = strPatch.substr(begin, nChars);
					}
					else if(strPatch.substr(pos2, 12)=="OSCWAVEFORM=")
					{
						string strValue = strPatch.substr(pos2+12, 2); // get 2 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 15)); // limit to range
						_OSCWaveform = value;
					}
					else if(strPatch.substr(pos2, 11)=="FMWAVEFORM=")
					{
						string strValue = strPatch.substr(pos2+11, 2); // get 2 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 15)); // limit to range
						_FMWaveform = value;
					}
					else if(strPatch.substr(pos2, 13)=="OSCFREQRATIO=")
					{
						string strValue = strPatch.substr(pos2+13, 4); // get 4 digits following '='
						char* pEnd;
						double value = strtod(strValue.c_str(), &pEnd);
						value = max(0.0, min(value, 16.0)); // limit to range
						_OSCFreqRatio = value;
					}
					else if(strPatch.substr(pos2, 12)=="FMFREQRATIO=")					
					{
						string strValue = strPatch.substr(pos2+12, 4); // get 4 digits following '='
						char* pEnd;
						double value = strtod(strValue.c_str(), &pEnd);
						value = max(0.0, min(value, 16.0)); // limit to range
						_FMFreqRatio = value;
					}
					else if(strPatch.substr(pos2, 9)=="MODDEPTH=")
					{
						string strValue = strPatch.substr(pos2+9, 4); // get 4 digits following '='
						char* pEnd;
						double value = strtod(strValue.c_str(), &pEnd);
						value = max(0.0, min(value, 16.0)); // limit to range
						_modDepth = value;
					}
					else if(strPatch.substr(pos2, 14)=="OSCATTACKTIME=")
					{
						string strValue = strPatch.substr(pos2+14, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_OSCAttackTime = value;
					}
					else if(strPatch.substr(pos2, 12)=="OSCPEAKTIME=")
					{
						string strValue = strPatch.substr(pos2+12, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_OSCPeakTime = value;
					}
					else if(strPatch.substr(pos2, 13)=="OSCDECAYTIME=")					
					{
						string strValue = strPatch.substr(pos2+13, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_OSCDecayTime = value;
					}
					else if(strPatch.substr(pos2, 15)=="OSCRELEASETIME=")
					{
						string strValue = strPatch.substr(pos2+15, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_OSCReleaseTime = value;
					}
					else if(strPatch.substr(pos2, 13)=="OSCPEAKLEVEL=")
					{
						string strValue = strPatch.substr(pos2+13, 4); // get 4 digits following '='
						double value = atof(strValue.c_str());
						value = max(0.0, min(value, 1.0)); // limit to range
						_OSCPeakLevel = value;
					}
					else if(strPatch.substr(pos2, 16)=="OSCSUSTAINLEVEL=")
					{
						string strValue = strPatch.substr(pos2+16, 4); // get 4 digits following '='
						double value = atof(strValue.c_str());
						value = max(0.0, min(value, 1.0)); // limit to range
						_OSCSustainLevel = value;
					}
					else if(strPatch.substr(pos2, 13)=="FMATTACKTIME=")
					{
						string strValue = strPatch.substr(pos2+13, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_FMAttackTime = value;
					}
					else if(strPatch.substr(pos2, 11)=="FMPEAKTIME=")
					{
						string strValue = strPatch.substr(pos2+11, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_FMPeakTime = value;
					}
					else if(strPatch.substr(pos2, 12)=="FMDECAYTIME=")					
					{
						string strValue = strPatch.substr(pos2+12, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_FMDecayTime = value;
					}
					else if(strPatch.substr(pos2, 14)=="FMRELEASETIME=")
					{
						string strValue = strPatch.substr(pos2+14, 4); // get 4 digits following '='
						int value = atoi(strValue.c_str());
						value = max(0, min(value, 9999)); // limit to range
						_FMReleaseTime = value;
					}
					else if(strPatch.substr(pos2, 12)=="FMPEAKLEVEL=")
					{
						string strValue = strPatch.substr(pos2+12, 4); // get 4 digits following '='
						double value = atof(strValue.c_str());
						value = max(0.0, min(value, 1.0)); // limit to range
						_FMPeakLevel = value;
					}
					else if(strPatch.substr(pos2, 15)=="FMSUSTAINLEVEL=")
					{
						string strValue = strPatch.substr(pos2+15, 4); // get 4 digits following '='
						double value = atof(strValue.c_str());
						value = max(0.0, min(value, 1.0)); // limit to range
						_FMSustainLevel = value;
					}					
						
					
					pos2++;
					
					// reached end of individual patch data string. now push these params into one new patch
					if(strPatch.at(pos2)=='$' || pos2>=strPatch.length())
					{
						patch.push_back(Patch());
						int i = patch.size()-1;
						patch[i].name = _name;
						patch[i].OSCWaveform = _OSCWaveform;
						patch[i].FMWaveform = _FMWaveform;
						patch[i].OSCFreqRatio = _OSCFreqRatio;
						patch[i].FMFreqRatio = _FMFreqRatio;
						patch[i].modDepth = _modDepth;
						patch[i].OSCAttackTime = _OSCAttackTime;
						patch[i].OSCPeakTime = _OSCPeakTime;
						patch[i].OSCDecayTime = _OSCDecayTime;
						patch[i].OSCReleaseTime = _OSCReleaseTime;
						patch[i].OSCPeakLevel = _OSCPeakLevel;
						patch[i].OSCSustainLevel = _OSCSustainLevel;
						patch[i].FMAttackTime = _FMAttackTime;
						patch[i].FMPeakTime = _FMPeakTime;
						patch[i].FMDecayTime = _FMDecayTime;
						patch[i].FMReleaseTime = _FMReleaseTime;
						patch[i].FMPeakLevel = _FMPeakLevel;
						patch[i].FMSustainLevel = _FMSustainLevel;
						
						presetDone = true;
					}
				}
				pos++;
			}
			// 'PRESET_END' not found... will fast forward
			else
				pos++;
		}
		// 'PRESET_BEGIN' not found at pos yet
		else
			pos++;
		
		// check if end of string has been reached
		if(str.at(pos)=='$' || pos>=str.length())
			allDone = true;	
	}
	
	nPresets = patch.size();
}

std::string Player::getpwd()
{
	string strReturn;
	
	#ifdef _WIN32
	TCHAR currentDir[MAX_PATH];
	GetCurrentDirectory( MAX_PATH, currentDir );
	strReturn = currentDir;
	#endif
	
	return strReturn;
}

bool Player::dirExists(const std::string &dirPath)
{
	string path = dirPath;
	#if _WIN32
		DWORD dwAttrib = GetFileAttributes(path.c_str());
		return (bool)(dwAttrib != INVALID_FILE_ATTRIBUTES && 
			(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
	#elif
		bool result;
		DIR* dir = opendir(path.c_str());
		if (dir)
		{
			result = true;
			/* Directory exists. */
			closedir(dir);
		}
		else if (ENOENT == errno)
		{
			result = false;
			/* Directory does not exist. */
		}
		else
		{
			result = false;
			cout << "Error - opendir(): " << path << endl;
			/* opendir() failed for some other reason. */
		}
		return result;
	#endif
}

OSC* Player::getOSCObject()
{	return &osc; }

FM* Player::getFMObject()
{	return osc.getFMObject(); }