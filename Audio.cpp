#include "iostream" // DEBUG
#include "Audio.h"

using namespace std;

const int Audio::SAMPLE_RATE = 44100;
const int Audio::FRAMES_PER_BUFFER = 128;

Audio::Audio()
{
	initialize();
}

// real callback function
int Audio::audioCallback(
								const void *inputBuffer,
								void *outputBuffer,
								unsigned long framesPerBuffer,
								const PaStreamCallbackTimeInfo* timeInfo,
								PaStreamCallbackFlags statusFlags
							)
{	
	float *out = (float*)(outputBuffer);
	int nBufferFrames = (int)framesPerBuffer;

	(void) inputBuffer;
	(void) timeInfo;
	(void) statusFlags;
	
	for(int i=0; i<nBufferFrames; i++)
	{
		// float r = ((float) rand() / (RAND_MAX));
		// r -= 0.5f;
		*out = player->getMix();
		out++;	
	}
	
	return paContinue;
}

void Audio::initialize()
{	
	errorRaised = false;
	err = Pa_Initialize();
	if( err != paNoError ) raiseError(err);
	
	// set up output parameters
    outputParameters.device = Pa_GetDefaultOutputDevice(); // use default device
    if (outputParameters.device == paNoDevice)
	{
      cout << "Error: No default output device." << "\r\n";
    }
    outputParameters.channelCount = 1; // mono
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
	
	// open audio stream for recording using default device
	err = Pa_OpenStream(
		&stream,
		NULL, // no input
		&outputParameters,
		SAMPLE_RATE,
		FRAMES_PER_BUFFER,
		paClipOff,
		audioCallbackReroute,
		this
	);
	
	if( err != paNoError ) raiseError(err);
	
	cout << "Audio iniialized!\n";
}

void Audio::start()
{
	// start the audio stream!
	err = Pa_StartStream(stream);
	if(err != paNoError) raiseError(err);
}

void Audio::terminate()
{
	err = Pa_CloseStream( stream );
	if(err != paNoError) raiseError(err);	
	err = Pa_Terminate( );
	if(err != paNoError) raiseError(err);	
}

void Audio::raiseError(PaError e)
{
	cout << "Port audio error:\n";
	string errText = Pa_GetErrorText(e);
	cout << errText << "\r\n";
	errorRaised = true;
}

void Audio::bindPlayer(Player* p)
{
	player = p;
}
