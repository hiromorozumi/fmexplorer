#include <iostream>
#include <fstream>
#include <string>
#include <gtk/gtk.h>
#include "Audio.h"
#include "hirolib.h"

using namespace std;

#ifdef _WIN32
	static const std::string SL = "\\";
#else
	static const std::string SL = "/";
#endif

bool exitApp;
Player p;
Audio audio;
OSC* osc;
FM* fm;

std::string strWaveform[] = { "0 - Sine Wave", "1 - Square Wave", "2 - Sawtooth Wave", "3 - Triangle Wave",
							"4 - *",  "5 - *",   "6 - *", "7 - *",
							"8 - *",  "9 - *",  "10 - *", "11 - *",
							"12 - *", "13 - *", "14 - *", "15 - *"
};

	// gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformOSCCombo), NULL, "0 - Sine Wave");
	// gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformOSCCombo), NULL, "1 - Square Wave");
	// gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformOSCCombo), NULL, "2 - Sawtooth Wave");
	// gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformOSCCombo), NULL, "3 - Triangle Wave");

int getValueFromString(const std::string &str);
void setOSCWaveform(GtkComboBox *widget, gpointer user_data);
void setOSCFreqRatio(GtkSpinButton *widget, gpointer user_data);
void setOSCAttackTime(GtkRange *range, gpointer user_data);
void setOSCPeakTime(GtkRange *range, gpointer user_data);
void setOSCDecayTime(GtkRange *range, gpointer user_data);
void setOSCReleaseTime(GtkRange *range, gpointer user_data);
void setOSCPeakLevel(GtkRange *range, gpointer user_data);
void setOSCSustainLevel(GtkRange *range, gpointer user_data);

void setFMWaveform(GtkComboBox *widget, gpointer user_data);
void setFMFreqRatio(GtkSpinButton *widget, gpointer user_data);
void setFMAttackTime(GtkRange *range, gpointer user_data);
void setFMPeakTime(GtkRange *range, gpointer user_data);
void setFMDecayTime(GtkRange *range, gpointer user_data);
void setFMReleaseTime(GtkRange *range, gpointer user_data);
void setFMPeakLevel(GtkRange *range, gpointer user_data);
void setFMSustainLevel(GtkRange *range, gpointer user_data);
void setFMModulationDepth(GtkSpinButton *widget, gpointer user_data);

void exportParameters(GtkButton *button, gpointer user_data);
void setCurrentGUIParams();
void setPreset(GtkSpinButton *widget, gpointer user_data);

void quitApp();

GtkWidget* window;
// GtkWidget* fixed;

// OSC envelope controls

GtkWidget* waveformOSCLabel;	
GtkWidget* waveformOSCCombo;

GtkWidget* freqRatioOSCLabel;
GtkWidget* freqRatioOSCButton;

GtkWidget* attackTimeOSCLabel;
GtkWidget* peakTimeOSCLabel;
GtkWidget* decayTimeOSCLabel;
GtkWidget* releaseTimeOSCLabel;
GtkWidget* peakLevelOSCLabel;
GtkWidget* sustainLevelOSCLabel;

GtkWidget* attackTimeOSCScale;
GtkWidget* peakTimeOSCScale;
GtkWidget* decayTimeOSCScale;
GtkWidget* releaseTimeOSCScale;
GtkWidget* peakLevelOSCScale;
GtkWidget* sustainLevelOSCScale;


// FM envelope controls

GtkWidget* waveformFMLabel;
GtkWidget* waveformFMCombo;

GtkWidget* freqRatioFMLabel;
GtkWidget* freqRatioFMButton;

GtkWidget* modDepthFMLabel;
GtkWidget* modDepthFMButton;

GtkWidget* attackTimeFMLabel;
GtkWidget* peakTimeFMLabel;
GtkWidget* decayTimeFMLabel;
GtkWidget* releaseTimeFMLabel;
GtkWidget* peakLevelFMLabel;
GtkWidget* sustainLevelFMLabel;

GtkWidget* attackTimeFMScale;
GtkWidget* peakTimeFMScale;
GtkWidget* decayTimeFMScale;
GtkWidget* releaseTimeFMScale;
GtkWidget* peakLevelFMScale;
GtkWidget* sustainLevelFMScale;

// general controls
GtkWidget* paramExportButton;
GtkWidget* presetNameLabel;
GtkWidget* presetButton;


int main(int argc, char** argv)
{
	exitApp = false;
	int patchNumber = 0;
	p.initialize();
	p.choosePreset(0);
	osc = p.getOSCObject();
	fm = p.getFMObject();
	audio.bindPlayer(&p);
	audio.start();
	
	gtk_init(&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(window), 620, 620);	
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	GtkWidget* fixed;
	fixed = gtk_fixed_new();
	gtk_container_add(GTK_CONTAINER(window), fixed);
	
	// OSC envelope controls

	waveformOSCLabel = gtk_label_new("OSC Wave");
	
	/*
	waveformOSCCombo = gtk_combo_box_new();
    GList *cbitemsOSC = NULL;
	for(int i=0;i<16;i++)
	{
		gchar* temp = g_strdup(strWaveform[i].c_str());		
		cbitemsOSC = g_list_append(cbitemsOSC, temp);
		g_free(temp);
	}
    gtk_combo_set_popdown_strings(GTK_COMBO_BOX(waveformOSCCombo), cbitemsOSC);
    gtk_entry_set_text (GTK_ENTRY (GTK_COMBO_BOX(waveformOSCCombo)->entry), strWaveform[0])
	*/
	
	waveformOSCCombo = gtk_combo_box_text_new();
	for(int i=0;i<16;i++)
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformOSCCombo), NULL, strWaveform[i].c_str());
	gtk_combo_box_set_active(GTK_COMBO_BOX(waveformFMCombo), 0);
	
	freqRatioOSCLabel = gtk_label_new("OSC FreqRatio");
	freqRatioOSCButton = gtk_spin_button_new_with_range(0.0, 16.0, 0.1);
	
	attackTimeOSCLabel = gtk_label_new("OSC Attack Time");
	peakTimeOSCLabel = gtk_label_new("OSC Peak Time");
	decayTimeOSCLabel = gtk_label_new("OSC Decay Time");
	releaseTimeOSCLabel = gtk_label_new("OSC Release Time");
	peakLevelOSCLabel = gtk_label_new("OSC Peak Level");
	sustainLevelOSCLabel = gtk_label_new("OSC Sustain Level");
	
	attackTimeOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	peakTimeOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	decayTimeOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);	
	releaseTimeOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	peakLevelOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 1.0, 0.01);
	sustainLevelOSCScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 1.0, 0.01);	

	
	// FM envelope controls

	waveformFMLabel = gtk_label_new("FM Wave");	
	waveformFMCombo = gtk_combo_box_text_new();

	waveformFMCombo = gtk_combo_box_text_new();
	for(int i=0;i<16;i++)
		gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(waveformFMCombo), NULL, strWaveform[i].c_str());
	gtk_combo_box_set_active(GTK_COMBO_BOX(waveformFMCombo), 0);
	
	freqRatioFMLabel = gtk_label_new("FM FreqRatio");
	freqRatioFMButton = gtk_spin_button_new_with_range(0.0, 16.0, 0.1);

	modDepthFMLabel = gtk_label_new("Modulation Depth");
	modDepthFMButton = gtk_spin_button_new_with_range(0.0, 10.0, 0.1);
	
	attackTimeFMLabel = gtk_label_new("FM Attack Time");
	peakTimeFMLabel = gtk_label_new("FM Peak Time");
	decayTimeFMLabel = gtk_label_new("FM Decay Time");
	releaseTimeFMLabel = gtk_label_new("FM Release Time");
	peakLevelFMLabel = gtk_label_new("FM Peak Level");
	sustainLevelFMLabel = gtk_label_new("FM Sustain Level");
	
	attackTimeFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	peakTimeFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	decayTimeFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);	
	releaseTimeFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 9999, 1);
	peakLevelFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 1.0, 0.01);
	sustainLevelFMScale = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0, 1.0, 0.01);

	// general controls
	
	paramExportButton = gtk_button_new_with_label("Export to txt File");
	presetButton = gtk_spin_button_new_with_range(0, 32, 1);
	presetNameLabel = gtk_label_new("Default");
	
	// layout for OSC controls
	
	int left = 20; int top = 20;

	gtk_fixed_put(GTK_FIXED(fixed), waveformOSCLabel,   left, top + 7);	
	gtk_fixed_put(GTK_FIXED(fixed), waveformOSCCombo,   left + 110, top);

	gtk_fixed_put(GTK_FIXED(fixed), freqRatioOSCLabel,   left + 60, top + 40 + 5);	
	gtk_fixed_put(GTK_FIXED(fixed), freqRatioOSCButton,   left + 60 + 110, top + 40);	
	
	int sliderWidth = 260; int sliderHeight = 42;
	int top2 = 142; int w = 80; int inc = 68; int yAdd = 12;
	
	gtk_fixed_put(GTK_FIXED(fixed), attackTimeOSCLabel,   left + w*0, top2 + inc*0);
	gtk_fixed_put(GTK_FIXED(fixed), attackTimeOSCScale,   left + w*0, top2 + inc*0 + yAdd);
	gtk_widget_set_size_request(attackTimeOSCScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), peakTimeOSCLabel,     left + w*0, top2 + inc*1);
	gtk_fixed_put(GTK_FIXED(fixed), peakTimeOSCScale,     left + w*0, top2 + inc*1 + yAdd);
	gtk_widget_set_size_request(peakTimeOSCScale, sliderWidth, sliderHeight);
	
	gtk_fixed_put(GTK_FIXED(fixed), decayTimeOSCLabel,    left + w*0, top2 + inc*2);
	gtk_fixed_put(GTK_FIXED(fixed), decayTimeOSCScale,    left + w*0, top2 + inc*2 + yAdd);
	gtk_widget_set_size_request(decayTimeOSCScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), releaseTimeOSCLabel,  left + w*0, top2 + inc*3);
	gtk_fixed_put(GTK_FIXED(fixed), releaseTimeOSCScale,  left + w*0, top2 + inc*3 + yAdd);
	gtk_widget_set_size_request(releaseTimeOSCScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), peakLevelOSCLabel,    left + w*0, top2 + inc*4);
	gtk_fixed_put(GTK_FIXED(fixed), peakLevelOSCScale,    left + w*0, top2 + inc*4 + yAdd);
	gtk_widget_set_size_request(peakLevelOSCScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), sustainLevelOSCLabel, left + w*0, top2 + inc*5);
	gtk_fixed_put(GTK_FIXED(fixed), sustainLevelOSCScale, left + w*0, top2 + inc*5 + yAdd);
	gtk_widget_set_size_request(sustainLevelOSCScale, sliderWidth, sliderHeight);

	// layout for FM controls
	
	int left2 = 330;
	
	gtk_fixed_put(GTK_FIXED(fixed), waveformFMLabel,   left2, top + 7);	
	gtk_fixed_put(GTK_FIXED(fixed), waveformFMCombo,   left2 + 110, top);

	gtk_fixed_put(GTK_FIXED(fixed), freqRatioFMLabel,   left2 + 42, top + 40 + 5);	
	gtk_fixed_put(GTK_FIXED(fixed), freqRatioFMButton,   left2 + 42 + 130, top + 40);

	gtk_fixed_put(GTK_FIXED(fixed), modDepthFMLabel,   left2 + 42, top + 70 + 5);	
	gtk_fixed_put(GTK_FIXED(fixed), modDepthFMButton,   left2 + 42 + 130, top + 70);
	
	gtk_fixed_put(GTK_FIXED(fixed), attackTimeFMLabel,   left2 + w*0, top2 + inc*0);
	gtk_fixed_put(GTK_FIXED(fixed), attackTimeFMScale,   left2 + w*0, top2 + inc*0 + yAdd);
	gtk_widget_set_size_request(attackTimeFMScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), peakTimeFMLabel,     left2 + w*0, top2 + inc*1);
	gtk_fixed_put(GTK_FIXED(fixed), peakTimeFMScale,     left2 + w*0, top2 + inc*1 + yAdd);
	gtk_widget_set_size_request(peakTimeFMScale, sliderWidth, sliderHeight);
	
	gtk_fixed_put(GTK_FIXED(fixed), decayTimeFMLabel,    left2 + w*0, top2 + inc*2);
	gtk_fixed_put(GTK_FIXED(fixed), decayTimeFMScale,    left2 + w*0, top2 + inc*2 + yAdd);
	gtk_widget_set_size_request(decayTimeFMScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), releaseTimeFMLabel,  left2 + w*0, top2 + inc*3);
	gtk_fixed_put(GTK_FIXED(fixed), releaseTimeFMScale,  left2 + w*0, top2 + inc*3 + yAdd);
	gtk_widget_set_size_request(releaseTimeFMScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), peakLevelFMLabel,    left2 + w*0, top2 + inc*4);
	gtk_fixed_put(GTK_FIXED(fixed), peakLevelFMScale,    left2 + w*0, top2 + inc*4 + yAdd);
	gtk_widget_set_size_request(peakLevelFMScale, sliderWidth, sliderHeight);

	gtk_fixed_put(GTK_FIXED(fixed), sustainLevelFMLabel, left2 + w*0, top2 + inc*5);
	gtk_fixed_put(GTK_FIXED(fixed), sustainLevelFMScale, left2 + w*0, top2 + inc*5 + yAdd);
	gtk_widget_set_size_request(sustainLevelFMScale, sliderWidth, sliderHeight);

	// layout for general controls ... 
	
	int top3 = 575;
	gtk_fixed_put(GTK_FIXED(fixed), presetButton, left, top3);
	gtk_fixed_put(GTK_FIXED(fixed), presetNameLabel, left + 100, top3);
	gtk_fixed_put(GTK_FIXED(fixed), paramExportButton, 456, top3);
	
	// connect signals to all widgets...
	
	g_signal_connect (freqRatioOSCButton, "value-changed", G_CALLBACK(setOSCFreqRatio), NULL);	
	g_signal_connect (waveformOSCCombo, "changed", G_CALLBACK(setOSCWaveform), NULL);
	
	g_signal_connect (attackTimeOSCScale,  "value-changed", G_CALLBACK(setOSCAttackTime), NULL);	
	g_signal_connect (peakTimeOSCScale,    "value-changed", G_CALLBACK(setOSCPeakTime), NULL);	
	g_signal_connect (decayTimeOSCScale,   "value-changed", G_CALLBACK(setOSCDecayTime), NULL);	
	g_signal_connect (releaseTimeOSCScale, "value-changed", G_CALLBACK(setOSCReleaseTime), NULL);	
	g_signal_connect (peakLevelOSCScale,   "value-changed", G_CALLBACK(setOSCPeakLevel), NULL);	
	g_signal_connect (sustainLevelOSCScale,"value-changed", G_CALLBACK(setOSCSustainLevel), NULL);	
	
	g_signal_connect (freqRatioFMButton, "value-changed", G_CALLBACK(setFMFreqRatio), NULL);	
	g_signal_connect (waveformFMCombo, "changed", G_CALLBACK(setFMWaveform), NULL);
	
	g_signal_connect (attackTimeFMScale,  "value-changed", G_CALLBACK(setFMAttackTime), NULL);	
	g_signal_connect (peakTimeFMScale,    "value-changed", G_CALLBACK(setFMPeakTime), NULL);	
	g_signal_connect (decayTimeFMScale,   "value-changed", G_CALLBACK(setFMDecayTime), NULL);	
	g_signal_connect (releaseTimeFMScale, "value-changed", G_CALLBACK(setFMReleaseTime), NULL);	
	g_signal_connect (peakLevelFMScale,   "value-changed", G_CALLBACK(setFMPeakLevel), NULL);	
	g_signal_connect (sustainLevelFMScale,"value-changed", G_CALLBACK(setFMSustainLevel), NULL);
	g_signal_connect (modDepthFMButton, "value-changed", G_CALLBACK(setFMModulationDepth), NULL);
	
	g_signal_connect (paramExportButton, "clicked", G_CALLBACK(exportParameters), NULL);
	g_signal_connect (presetButton, "value-changed", G_CALLBACK(setPreset), NULL);
	g_signal_connect (window, "delete-event", G_CALLBACK (quitApp), NULL);
	
	// connect key event signals
	// g_signal_connect(window, "key-release-event", G_CALLBACK(handleKeyRelease), NULL);
	// g_signal_connect(window, "key-press-event", G_CALLBACK(handleKeyPress), NULL);
	
	// show all widgets ...
	
	gtk_widget_show(fixed);

	gtk_widget_show(waveformOSCLabel);
	gtk_widget_show(waveformOSCCombo);
	gtk_widget_show(freqRatioOSCLabel);
	gtk_widget_show(freqRatioOSCButton);
	
	gtk_widget_show(attackTimeOSCLabel);
	gtk_widget_show(peakTimeOSCLabel);
	gtk_widget_show(decayTimeOSCLabel);
	gtk_widget_show(releaseTimeOSCLabel);
	gtk_widget_show(peakLevelOSCLabel);
	gtk_widget_show(sustainLevelOSCLabel);
	
	gtk_widget_show(attackTimeOSCScale);
	gtk_widget_show(peakTimeOSCScale);
	gtk_widget_show(decayTimeOSCScale);
	gtk_widget_show(releaseTimeOSCScale);
	gtk_widget_show(peakLevelOSCScale);
	gtk_widget_show(sustainLevelOSCScale);

	gtk_widget_show(waveformFMLabel);
	gtk_widget_show(waveformFMCombo);
	gtk_widget_show(freqRatioFMLabel);
	gtk_widget_show(freqRatioFMButton);
	gtk_widget_show(modDepthFMLabel);
	gtk_widget_show(modDepthFMButton);
	
	gtk_widget_show(attackTimeFMLabel);
	gtk_widget_show(peakTimeFMLabel);
	gtk_widget_show(decayTimeFMLabel);
	gtk_widget_show(releaseTimeFMLabel);
	gtk_widget_show(peakLevelFMLabel);
	gtk_widget_show(sustainLevelFMLabel);
	
	gtk_widget_show(attackTimeFMScale);
	gtk_widget_show(peakTimeFMScale);
	gtk_widget_show(decayTimeFMScale);
	gtk_widget_show(releaseTimeFMScale);
	gtk_widget_show(peakLevelFMScale);
	gtk_widget_show(sustainLevelFMScale);
	
	gtk_widget_show(presetButton);
	gtk_widget_show(presetNameLabel);
	gtk_widget_show(paramExportButton);
	
	gtk_widget_show(window);
	
	//
	
	setCurrentGUIParams();
	
	// finally, run GTK application!
	
	while(!exitApp)
	{
		gtk_main_iteration_do(FALSE);
		
		if(kbd::esc())
		{
			while(kbd::esc()){}
			exitApp = true;
		}
		else if(kbd::z())
		{	p.setNote(48); while(kbd::z()){} }
		else if(kbd::s())
		{	p.setNote(49); while(kbd::s()){} }		
		else if(kbd::x())
		{	p.setNote(50); while(kbd::x()){} }
		else if(kbd::d())
		{	p.setNote(51); while(kbd::d()){} }		
		else if(kbd::c())
		{	p.setNote(52); while(kbd::c()){} }
		else if(kbd::v())
		{	p.setNote(53); while(kbd::v()){} }
		else if(kbd::g())
		{	p.setNote(54); while(kbd::g()){} }
		else if(kbd::b())
		{	p.setNote(55); while(kbd::b()){} }		
		else if(kbd::h())
		{	p.setNote(56); while(kbd::h()){} }
		else if(kbd::n())
		{	p.setNote(57); while(kbd::n()){} }		
		else if(kbd::j())
		{	p.setNote(58); while(kbd::j()){} }
		else if(kbd::m())
		{	p.setNote(59); while(kbd::m()){} }		
		else if(kbd::comma())
		{	p.setNote(60); while(kbd::comma()){} }	
		else if(kbd::up())
		{
			while(kbd::up()){}
			patchNumber++;
			if(patchNumber>=p.nPresets)
				patchNumber = 0;
			p.choosePreset(patchNumber);
			setCurrentGUIParams();
		}
		else if(kbd::down())
		{
			while(kbd::down()){}
			patchNumber--;
			if(patchNumber<0)
				patchNumber = p.nPresets - 1;
			p.choosePreset(patchNumber);
			setCurrentGUIParams();
		}
		else
			p.noteOff();		
	}
	return 0;
	
}

////////////////////////////////////////////////////////

void setOSCWaveform(GtkComboBox *widget, gpointer user_data)
{	
	/*
	string str = (string)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget));
	int value = getValueFromString(str);
	if(value>=0)
	{
		cout << "OSC Waveform : " << str << ", value = " << value << endl;
		osc->setTable(value); p.OSCWaveform = value;
	}
	*/
	int value = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
	cout << "OSC Waveform : " << strWaveform[value] << ", value = " << value << endl;
	osc->setTable(value); p.OSCWaveform = value;
}

int getValueFromString(const std::string &str)
{
	int value = -1;
	if     (str.find("0 -")!=string::npos) value = 0;
	else if(str.find("1 -")!=string::npos) value = 1;
	else if(str.find("2 -")!=string::npos) value = 2;
	else if(str.find("3 -")!=string::npos) value = 3;
	else if(str.find("4 -")!=string::npos) value = 4;
	else if(str.find("5 -")!=string::npos) value = 5;
	else if(str.find("6 -")!=string::npos) value = 6;
	else if(str.find("7 -")!=string::npos) value = 7;
	else if(str.find("8 -")!=string::npos) value = 8;
	else if(str.find("9 -")!=string::npos) value = 9;
	else if(str.find("10 -")!=string::npos) value = 10;
	else if(str.find("11 -")!=string::npos) value = 11;
	else if(str.find("12 -")!=string::npos) value = 12;
	else if(str.find("13 -")!=string::npos) value = 13;
	else if(str.find("14 -")!=string::npos) value = 14;
	else if(str.find("15 -")!=string::npos) value = 15;	
	return value;
}

void setOSCFreqRatio(GtkSpinButton *widget, gpointer user_data)
{
	double value = (double)gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	cout << "OSC Freq Ratio = " << value << endl;
	osc->setOSCFreqRatio(value); p.OSCFreqRatio = value;
}

void setOSCAttackTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "OSC Attack Time = " << value << endl;	
	osc->setAttackTime(value); p.OSCAttackTime = value; }
	
void setOSCPeakTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "OSC Peak Time = " << value << endl;
	osc->setPeakTime(value); p.OSCPeakTime = value; }

void setOSCDecayTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "OSC Decay Time = " << value << endl;
	osc->setDecayTime(value); p.OSCDecayTime = value; }

void setOSCReleaseTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "OSC Release Time = " << value << endl;
	osc->setReleaseTime(value); p.OSCReleaseTime = value; }

void setOSCPeakLevel(GtkRange *range, gpointer user_data)
{	double value = (double)gtk_range_get_value(range); cout << "OSC Peak Level = " << value << endl;
	osc->setPeakLevel((float)value); p.OSCPeakLevel = (float)value; }

void setOSCSustainLevel(GtkRange *range, gpointer user_data)
{	double value = (double)gtk_range_get_value(range); cout << "OSC Sustain Level = " << value << endl;	
	osc->setSustainLevel((float)value); p.OSCSustainLevel = (float)value; }

// FM

void setFMWaveform(GtkComboBox *widget, gpointer user_data)
{	
	/*
	string str = (string)gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget));
	int value = getValueFromString(str);
	if(value>=0)
	{
		cout << "FM Waveform : " << str << ", value = " << value << endl;
		osc->setFMTable(value); p.FMWaveform = value;
	}
	*/
	int value = gtk_combo_box_get_active(GTK_COMBO_BOX(widget));
	cout << "FM Waveform : " << strWaveform[value] << ", value = " << value << endl;
	osc->setFMTable(value); p.FMWaveform = value;
}

void setFMFreqRatio(GtkSpinButton *widget, gpointer user_data)
{
	double value = (double)gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	cout << "FM Freq Ratio = " << value << endl;
	osc->setFMFreqRatio(value); p.FMFreqRatio = value;
}

void setFMAttackTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "FM Attack Time = " << value << endl;	
	fm->setAttackTime(value); p.FMAttackTime = value; }
	
void setFMPeakTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "FM Peak Time = " << value << endl;
	fm->setPeakTime(value); p.FMPeakTime = value; }

void setFMDecayTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "FM Decay Time = " << value << endl;
	fm->setDecayTime(value); p.FMDecayTime = value; }

void setFMReleaseTime(GtkRange *range, gpointer user_data)
{	int value = (int)gtk_range_get_value(range); cout << "FM Release Time = " << value << endl;	
	fm->setReleaseTime(value); p.FMReleaseTime = value; }

void setFMPeakLevel(GtkRange *range, gpointer user_data)
{	double value = (double)gtk_range_get_value(range); cout << "FM Peak Level = " << value << endl;	
	fm->setPeakLevel((float)value); p.FMPeakLevel = (float)value; }

void setFMSustainLevel(GtkRange *range, gpointer user_data)
{	double value = (double)gtk_range_get_value(range); cout << "FM Sustain Level = " << value << endl;	
	fm->setSustainLevel((float)value); p.FMSustainLevel = (float)value; }

void setFMModulationDepth(GtkSpinButton *widget, gpointer user_data)
{
	double value = (double)gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	cout << "FM Mod depth = " << value << endl;
	osc->setFMModulationDepth(value); p.modDepth = value;	
}	
	
void exportParameters(GtkButton *button, gpointer user_data)
{
	// get timestamp string
	
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];
	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%Y-%m-%d_%I-%M-%S",timeinfo);
	std::string strTimeStamp(buffer);
	
	// populate data string to output
	
	string str = "////////////////////////////////////////\n";
	str += "\n";
	str += "Timestamp = " + strTimeStamp + "\n\n";
	str += "PRESET_BEGIN\n\n";
	str += "\tNAME=''\n\n";
	str += "\tOSCWAVEFORM=" + toStr(p.OSCWaveform) + "\n";
	str += "\tFMWAVEFORM=" + toStr(p.FMWaveform) + "\n";
	str += "\tOSCFREQRATIO=" + toStr(p.OSCFreqRatio) + "\n";
	str += "\tFMFREQRATIO=" + toStr(p.FMFreqRatio) + "\n";
	str += "\tMODDEPTH=" + toStr(p.modDepth) + "\n\n";
	str += "\tOSCATTACKTIME=" + toStr(p.OSCAttackTime) + "\n";
	str += "\tOSCPEAKTIME=" + toStr(p.OSCPeakTime) + "\n";
	str += "\tOSCDECAYTIME=" + toStr(p.OSCDecayTime) + "\n";
	str += "\tOSCRELEASETIME=" + toStr(p.OSCReleaseTime) + "\n";
	str += "\tOSCPEAKLEVEL=" + toStr(p.OSCPeakLevel) + "\n";
	str += "\tOSCSUSTAINLEVEL=" + toStr(p.OSCSustainLevel) + "\n\n";
	str += "\tFMATTACKTIME=" + toStr(p.FMAttackTime) + "\n";
	str += "\tFMPEAKTIME=" + toStr(p.FMPeakTime) + "\n";
	str += "\tFMDECAYTIME=" + toStr(p.FMDecayTime) + "\n";
	str += "\tFMRELEASETIME=" + toStr(p.FMReleaseTime) + "\n";
	str += "\tFMPEAKLEVEL=" + toStr(p.FMPeakLevel) + "\n";
	str += "\tFMSUSTAINLEVEL=" + toStr(p.FMSustainLevel) + "\n\n";
	str += "PRESET_END\n\n";

	// make sure there is an output folder
	string outputDir = file::getpwd() + SL + "export";
	if(!file::dirExists(outputDir))
	{
		cout << "Create directory: " << outputDir << endl;
		file::makeDir(outputDir);
	}
	
	// save this data string to file - named with current time stamp
	
	string filename = strTimeStamp + ".txt";
	cout << "Export parameters to: " << filename << endl;
	string filePath = outputDir + SL + filename;
	
	ofstream outFile(filePath);
	if(!outFile)
		cout << "Error opening output file: " << filePath << endl;
	else
		outFile << str;
	outFile.close();

	// 
	#ifdef _WIN32
		string commandStr = "start notepad.exe " + filePath;
		system(commandStr.c_str());
	#else
		
	#endif
}

void setCurrentGUIParams()
{
	gtk_range_set_value(GTK_RANGE(attackTimeOSCScale), (gdouble)p.OSCAttackTime);
	gtk_range_set_value(GTK_RANGE(peakTimeOSCScale), (gdouble)p.OSCPeakTime);
	gtk_range_set_value(GTK_RANGE(decayTimeOSCScale), (gdouble)p.OSCDecayTime);
	gtk_range_set_value(GTK_RANGE(releaseTimeOSCScale), (gdouble)p.OSCReleaseTime);
	gtk_range_set_value(GTK_RANGE(peakLevelOSCScale), (gdouble)p.OSCPeakLevel);
	gtk_range_set_value(GTK_RANGE(sustainLevelOSCScale), (gdouble)p.OSCSustainLevel);

	gtk_range_set_value(GTK_RANGE(attackTimeFMScale), (gdouble)p.FMAttackTime);
	gtk_range_set_value(GTK_RANGE(peakTimeFMScale), (gdouble)p.FMPeakTime);
	gtk_range_set_value(GTK_RANGE(decayTimeFMScale), (gdouble)p.FMDecayTime);
	gtk_range_set_value(GTK_RANGE(releaseTimeFMScale), (gdouble)p.FMReleaseTime);
	gtk_range_set_value(GTK_RANGE(peakLevelFMScale), (gdouble)p.FMPeakLevel);
	gtk_range_set_value(GTK_RANGE(sustainLevelFMScale), (gdouble)p.FMSustainLevel);
	
	gtk_combo_box_set_active(GTK_COMBO_BOX(waveformOSCCombo), (gdouble)p.OSCWaveform);	
	gtk_combo_box_set_active(GTK_COMBO_BOX(waveformFMCombo),  (gdouble)p.FMWaveform);

	cout << "OSCWaveform = " << p.OSCWaveform << endl;
	cout << "FMWaveform = " << p.FMWaveform << endl;
	
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(freqRatioOSCButton), (gdouble)p.OSCFreqRatio);		
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(freqRatioFMButton), (gdouble)p.FMFreqRatio);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(modDepthFMButton), (gdouble)p.modDepth);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(presetButton), (gdouble)p.currentPreset);	
	gtk_label_set_text(GTK_LABEL(presetNameLabel), (p.presetName).c_str());
}

void setPreset(GtkSpinButton *widget, gpointer user_data)
{
	int value = (int)gtk_spin_button_get_value(GTK_SPIN_BUTTON(widget));
	if(value>=p.nPresets)
	{
		value = p.nPresets-1;
		if(value<=0) value = 0;
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(presetButton), (gdouble)value);			
	}
	else if(value<0)
	{
		value = 0;
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(presetButton), (gdouble)value);			
	}
	p.choosePreset(value);
	cout << "Set preset = " << value << ", " << p.presetName << endl;
	setCurrentGUIParams();
	
	// DEBUG
	cout << "patch[" << p.currentPreset << "].OSCWaveform = " << p.patch[p.currentPreset].OSCWaveform << endl;
	cout << "patch[" << p.currentPreset << "].FMWaveform = " << p.patch[p.currentPreset].FMWaveform << endl;
	cout << "patch[" << p.currentPreset << "].OSCSustainLevel = " << p.patch[p.currentPreset].OSCSustainLevel << endl;
}

void quitApp()
{	exitApp = true; }